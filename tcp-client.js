var net = require('net');

var client = new net.Socket();
client.connect(101, 'localhost', function() {
	console.log('Connected');
    client.write('factory_reset\n');
    setTimeout(function(){
        console.log('Close connection now...');
        client.destroy();
    }, 5000);
});

client.on('data', function(data) {
	console.log('Received: ' + data);
});

client.on('close', function() {
	console.log('Connection closed');
});
